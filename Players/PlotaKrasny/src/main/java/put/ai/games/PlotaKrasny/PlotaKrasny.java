/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package put.ai.games.PlotaKrasny;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import put.ai.games.game.Board;
import put.ai.games.game.Move;
import put.ai.games.game.Player;
import put.ai.games.game.moves.MoveMove;
import put.ai.games.game.moves.PlaceMove;
import put.ai.games.snort.impl.SnortDropMove;


public class PlotaKrasny extends Player {

    public static void main(String[] args) {}

    private Random random = new Random(0xdeadbeef);

    int[][] board = new int[11][11];
    @Override
    public String getName() {
        return "Michał Płocki 132308 Maciej Kraśny 132260";
    }


    public void prepareBoard(Board b){
        for(int i=0;i<11;i++){
            for(int j=0;j<11;j++){
                if(b.getState(i,j) == Color.EMPTY) board[i][j] = 0;
                else if(b.getState(i,j) == Color.PLAYER1) board[i][j] = 1;
                else if(b.getState(i,j) == Color.PLAYER2) board[i][j] = 2;
            }
        }
    }

    public Boolean check(int i, int j, int player){
        int enemy;
        int count = 0;
        if(player == 1) enemy = 2;
        else enemy = 1;

        /*up*/
        if(j == 0 ) count +=1;
        else {
            if (board[i][j - 1] != enemy) count +=1;
        }
        /*down*/
        if(j==10) count +=1;
        else {
            if(board[i][j+1] != enemy) count +=1;
        }
        /*left*/
        if(i == 0) count +=1;
        else {
            if(board[i-1][j] != enemy) count +=1;
        }
        /*right*/
        if(i == 10) count +=1;
        else {
            if(i == 11 || board[i+1][j] != enemy) count +=1;
        }

        if(count == 4) return true;
        else return false;
    }

    public Integer rateState(int player) {
        int result = 0;
        for(int i=0; i<11;i++){
            for(int j=0; j<11;j++){
                if(board[i][j] == 0 ){
                    if(check(i,j,player)){
                        result += 1;
                    }
                }

            }
        }

        return result;
    }

    public int getMinIndex(ArrayList<Integer> listofvalues){
        int min = listofvalues.get(0);
        int index = 0;
        if(listofvalues.size()>1){
            for(int i=1;i<listofvalues.size();i++){
                if(listofvalues.get(i) < min){
                    min = listofvalues.get(i);
                    index = i;
                }
            }
        }
        return index;
    }

    @Override
    public Move nextMove(Board b) {
        int player, enemy;
        prepareBoard(b);
        List<Move> moves = b.getMovesFor(getColor());
        ArrayList<Integer> listofvalues = new ArrayList<Integer>();
        if(getColor() == Color.PLAYER1){
            player = 1;
            enemy = 2;
        }
        else{
            player = 2;
            enemy = 1;
        }

        for(int i=0;i<moves.size();i++){
            if(moves.get(i) instanceof PlaceMove) {
                PlaceMove tmp = (PlaceMove) moves.get(i);
                board[tmp.getX()][tmp.getY()] = player;

                listofvalues.add(rateState(enemy));
                board[tmp.getX()][tmp.getY()] = 0;
            }
            else if(moves.get(i) instanceof MoveMove) {
                MoveMove tmp = (MoveMove) moves.get(i);
                board[tmp.getDstX()][tmp.getDstY()] = player;
                listofvalues.add(rateState(enemy));
                board[tmp.getDstX()][tmp.getDstY()] = 0;
            }
        }

        return  moves.get(getMinIndex(listofvalues));
    }
}

